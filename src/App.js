import { ChatEngine} from 'react-chat-engine';

import LoginForm from './component/LoginForm';

import './App.css';
import ChatFeed from './component/ChatFeed';
const App = () => {
    if(!localStorage.getItem('username')) return <LoginForm/>
    return(
        <ChatEngine
        height="100vh"
        projectID='eadfee17-7dfa-4678-82f4-b54d17961189'
		userName={localStorage.getItem('username')}
		userSecret={localStorage.getItem('password')}
        renderChatFeed={(chatAppProps) => <ChatFeed {...chatAppProps}/>}

    />

    );
    
}
export default App;